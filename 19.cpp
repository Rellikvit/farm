#include <iostream>
#include <string>

class Animal
{
private:
	std::string phrase = "Hello";
public:
	virtual void Voice()
	{
		std::cout << phrase;
	}
	virtual ~Animal()
	{}
};

class Dog : public Animal
{
private:
	std::string phrase = "Woof!\n";
public:
	void Voice() override
	{
		std::cout << phrase;
	}
	~Dog()
	{}
};

class Cat : public Animal
{
private:
	std::string phrase = "Myau!\n";
public:
	void Voice() override
	{
		std::cout << phrase;
	}
	~Cat()
	{}
};

class Cow : public Animal
{
private:
	std::string phrase = "Mooo!\n";
public:
	void Voice() override
	{
		std::cout << phrase;
	}
	~Cow()
	{}
};


int main()
{
	Animal * Farm[] = {new Dog, new Cat, new Cow};
	for (int i = 0; i < 3;i++)
	{
		Farm[i]->Voice();
		delete Farm[i];
	}
}
